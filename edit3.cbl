       IDENTIFICATION DIVISION. 
       PROGRAM-ID. EDIT3.
       AUTHOR. Benjamas.

       ENVIRONMENT DIVISION. 
       CONFIGURATION SECTION. 
       SPECIAL-NAMES. 
           CURRENCY SIGN IS "£"
           CURRENCY SIGN IS "$"
           CURRENCY SIGN IS "¥".

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 DOLLAR-VALUE PIC 9999V99.

       01 PRN-DOLLAR-VALUE PIC $$$,$$9.99.
      * 01 PRN-YEN-VALUE PIC ¥¥¥,¥¥9.99.
      * 01 PRN-POUND-VALUE PIC £££,££9.99.

       01 DOLLAR-TO-POUND-RATE PIC 99V9(6) VALUE 0.640138.
       01 DOLLAR-TO-YEN-RATE PIC 99V9(6) VALUE 98.6600.

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "Enter a dollar value to convert :- " 
           WITH NO ADVANCING 

           ACCEPT DOLLAR-VALUE 
           MOVE DOLLAR-VALUE TO PRN-DOLLAR-VALUE 

      *     COMPUTE PRN-YEN-VALUE ROUNDED = 
      *     DOLLAR-VALUE *DOLLAR-TO-YEN-RATE 

      *     COMPUTE PRN-POUND-VALUE  ROUNDED = 
      *     DOLLAR-VALUE *DOLLAR-TO-POUND-RATE 

           DISPLAY "Dollar value   = " PRN-DOLLAR-VALUE 
      *     DISPLAY "Yen value      = " PRN-YEN-VALUE 
      *     DISPLAY "Pound value    = " PRN-POUND-VALUE 

           GOBACK 
           .
